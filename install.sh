#!/bin/bash

sudo apt-get update

# Install curl if not found
if ! type "curl" > /dev/null; then
  sudo apt-get install curl -y
fi

# Install git if not found
if ! type "git" > /dev/null; then
  sudo apt-get install git -y
fi

# Install nvm
curl https://raw.githubusercontent.com/creationix/nvm/v0.18.0/install.sh | sh
[ -s "/home/vagrant/.nvm/nvm.sh" ] && . "/home/vagrant/.nvm/nvm.sh"

# Install Node.js v0.10.33
nvm install 0.10.33
nvm alias default 0.10.33

# Install Grunt & Bower CLI tools
npm install -g grunt-cli bower

# Install RVM
curl -L https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm
echo "source ~/.rvm/scripts/rvm" >> ~/.bashrc

# Install Ruby
rvm install 2.1.3
rvm use 2.1.3 --default
echo "gem: --no-ri --no-rdoc" > ~/.gemrc

# Install Sass tools
gem install sass
gem install compass
gem install sass-globbing
gem install breakpoint
