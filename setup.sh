#!/bin/bash

# Prompt for repo url and dist path
while true
do
	read -p "Clone URL of your project: " src

	if [ ! -z "$src" ]; then
		git_repo=$(basename $src)
		repo_name=${git_repo%.*}
		break
	fi
done

read -p "Project destination path ($repo_name): " dist

if [ -z "$dist" ]; then
	dist=$repo_name
fi

# Clone project repo
git clone $src $dist

# Install VirtualBox if not found
if ! vb="$(type -p "virtualbox")" || [ -z "$vb" ]; then
	sudo apt-get install virtualbox -y
fi

# Install Vagrant if not found
if ! vg="$(type -p "vagrant")" || [ -z "$vg" ]; then
	sudo apt-get install vagrant -y
fi

# Add precise32 box if not found
if ! vagrant box list | grep "precise32" > /dev/null; then
	vagrant box add precise32 http://files.vagrantup.com/precise32.box
fi

# Init Vagrant
vagrant init precise32

# Sync project folder with Vagrant
head -n -1 Vagrantfile > Vagrantfile.tmp
echo "Vagrant.configure("2") do |config|" >> Vagrantfile.tmp
echo "  config.vm.box = \"precise32\"" >> Vagrantfile.tmp
echo "  config.vm.synced_folder \"$dist\", \"/home/vagrant/$repo_name\"" >> Vagrantfile.tmp
echo "end" >> Vagrantfile.tmp
rm Vagrantfile
mv Vagrantfile.tmp Vagrantfile

# Install tools
vagrant up
vagrant ssh -c "wget -O - https://bitbucket.org/sapientnyc/vagrantfed/raw/master/install.sh | bash"
vagrant reload

# Install project dependencies
vagrant ssh -c "cd /home/vagrant/$repo_name && npm install && bower install"
